package net.oschina.git.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import net.oschina.git.views.CloneRepository;
import net.oschina.git.views.Config;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
/**
 * Handler for opening the settings dialog.
 */
public class SettingsHandler extends AbstractHandler {
	
	/**
	 * Opens the settings dialog.
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow workbenchWindow = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		new Config(workbenchWindow.getShell()).open();
		return null;
	}
}
